\LoadClass{article}
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{cv}[2018/07/24 Ellis Judge Custom CV class (Adapted by James Meredith)]

\RequirePackage{titlesec}
%\RequirePackage{sectsty}
\titlespacing{\section}{0pt}{5pt}{5pt}
\titlespacing{\subsection}{0pt}{5pt}{0pt}

\renewcommand{\familydefault}{cmss}

\RequirePackage[hidelinks]{hyperref}
\RequirePackage[a4paper, margin=.5in]{geometry}
\RequirePackage{multicol}
\RequirePackage{parcolumns}
\RequirePackage{paracol}
\RequirePackage{fontawesome5}
\RequirePackage[skins]{tcolorbox}
\RequirePackage{pgffor}

\newtcbox{\keyword}{enhanced,nobeforeafter,tcbox raise base,boxrule=0.6pt,top=0mm,bottom=0mm,
	right=0mm,left=0mm,arc=2pt,boxsep=3pt,before upper={\vphantom{dlg}},
	colframe=white,coltext=black,colback=blue!7!white, fontupper=\footnotesize}

\MakeRobust\keyword

\ifdefined\pdfstringdefDisableCommands
\pdfstringdefDisableCommands{\def\keyword#1{'#1'}}
\fi

\RequirePackage{datetime}
\ddmmyyyydate

\RequirePackage{fancyhdr}
\pagestyle{fancy}
\renewcommand{\headrulewidth}{0pt}
\lhead{}
\chead{}
\rhead{}
\cfoot{}
%\rfoot{Last Updated: \today}

\setlength{\parindent}{0pt}
\setlength{\columnsep}{.5cm}

\titleformat{\section}
	{\LARGE\slshape\raggedright}
	{}{0em}{}
	[{\titlerule[.5pt]}]
	
\titleformat{\subsection}
	{\large\raggedright}
	{}{0em}{}
	[]

\newcommand{\datedsection}[2]{\section[#1]{#1 \hfill #2}}

\newcommand{\datedsubsection}[2]{\subsection[#1]{\textbf{#1} \hfill #2}}

\newcommand{\education}[2]{\subsection[#1]{\textbf{#1} \hfill #2}}

\newcommand{\name}[1]{\centerline{\Huge{#1}} \medskip}

\newcommand{\personal}[3]{\centerline{\faHome \ #1 {\Large\textperiodcentered} \href{mailto:#2}{\faEnvelope \ #2} {\Large\textperiodcentered} \faPhone \ #3}\medskip}

\newcommand{\prize}[2]{\faTrophy \ {#1} (#2)}

\newcommand{\website}[1]{\centerline{\href{#1}{\faCode \ #1}}\medskip}

\newcommand{\project}[2]{\datedsubsection{#1}{#2}}

\newcommand{\references}{\Large\bf References are available on request.}

\newenvironment{Languages}{\section{Programming Languages}\setlength{\columnsep}{-1.5in}\setlength{\multicolsep}{4.0pt}\begin{multicols}{2}\begin{itemize}}{\end{itemize}\end{multicols}}

\newenvironment{Technologies}{\section{Programming Technologies}\setlength{\columnsep}{-1.5in}\setlength{\multicolsep}{4.0pt}\begin{multicols}{3}\begin{itemize}}{\end{itemize}\end{multicols}}

\newenvironment{OperatingSystems}{\subsection{OperatingSystems}\begin{itemize}}{\end{itemize}}

\newcommand{\Repeat}[2]{% \repeat already defined
	\foreach \n in {1,...,#1}{#2}
}

\RequirePackage{tikz}
\newcommand\score[2]{%
	\hfill
	\ifnum#1>#2
	$#1 > #2$
	\else
	\ifnum#1<0
	$#1 < 0$
	\else
	\ifnum#2<0
	$#2 < 0$
	\else
	\tikz{%
		\ifx#20
		\else
		\foreach \i in {1,...,#2} {
			\filldraw[black!20] (\i ex,0) circle (0.4ex);
		};
		\fi
		\ifx#10
		\else
		\foreach \i in {1,...,#1} {
			\filldraw[black] (\i ex,0) circle (0.4ex);
		};
		\fi
	}
	\fi
	\fi
	\fi
	\smallskip
}
